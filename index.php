<?php
/*
 * @Author: Sathish
 * @Desc: load index and sorting algorithm
 */
ini_set('max_execution_time', 300); //300 seconds = 5 minutes


// Script start
$rustart = getrusage();

//load transport classes
require_once 'class/boardingCards.php';
require_once 'class/Transportation.php';

if ( isset( $argv[1] ) ) 
{
    $type = "tests/data/".$argv[1].".json";
}
elseif(isset( $_GET['type'] ))
{
    $type = "tests/data/".$_GET['type'].".json";
}
else
{
    $type = "tests/data/unsorted.json";
}


$transport = new sortboardingCards($type);

$transport->listboardingCards();


// Code ...

// Script end
function rutime($ru, $rus, $index) {
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

$ru = getrusage();

echo "<br>";
echo "This process used " . rutime($ru, $rustart, "utime") .
    " ms for its computations\n";
echo "It spent " . rutime($ru, $rustart, "stime") .
    " ms in system calls\n";
?>