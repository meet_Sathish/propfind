<?php
/**
 * Class TransportationTest
 *
 * @author    Sathish
 * @link      https://gitlab.com/sathishkumarb/tripsortdubi.git
 * @copyright null
 */

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once './class/boardingCards.php';
require_once './class/Transportation.php';

class TransportationTest extends TestCase
{
    
    /**
     * @var cards
     */
    protected $cards;    


     /**
     * @var cards
     */
    protected $obj;    
    

    # tests to laod anc construct the load json cards

    public function setUp()
    {

        $this->obj = new sortboardingCards("./data/broken.json");

        // $this->objtransport = new Transportation;
        
        $this->cards = $this->obj->getCards();

        //$this->obj = new boardingCards($this->sortedcards);
    
    }

    /** Test File not found json exception
     * return void
     */
    public function testFileNotFoundException()
    {
        $this->obj2 = new sortboardingCards("./data/nothing.json");

        // $this->objtransport = new Transportation;
        
        $this->cards2 = $this->obj2->getCards();

        
        $this->expectException(InvalidArgumentException::class);
           
       
    }
    
    /** Test invalid 
     * return void
     */
    public function testInvalidJsonException()
    {
        $this->obj3 = new sortboardingCards("./data/nothing.txt");

        // $this->objtransport = new Transportation;
        
        $this->cards3 = $this->obj3->getCards();
        
        
        $this->expectException(InvalidArgumentException::class);
            
        
    }


    /** Test if load direcotyr for jsn exits
     * return bool
     */

    public function testDirectoryFailure()
    {
        $this->assertDirectoryExists("./data/");
    }


    /** Testload file exits
     * return bool
     */

    public function testFileFailure()
    {
        $this->assertFileExists("./data/sorted.json");
    }



     /** Testload file exits
     * return bool
     */


    public function testjsonStructFailure()
    {
         $this->assertJsonStringEqualsJsonFile(
            "./data/sorted.json", json_encode(['departure' => 'ux'])
        );
    }

    /** Test Cards loaded are instance class sortboaridngcards
     * return void
     */
    public function testCardsIsValidInstance()
    {       

        $this->assertInstanceOf('sortboardingCards',$this->obj);
       
    }

     /** Test Cards loaded are instance and of same type ex: array
     * return bool
     */
    public function testCardsIsValidInstanceOf()
    {
         $this->assertContainsOnlyInstancesOf($this->obj, array());
       
    }

    /** Test if not abstract class and instance
     * return true
     */

    public function testFailureInstanceClassTest()
    {
        $this->assertContainsOnlyInstancesOf(
            $this->objtransport,
            [new Traintransport, new PublicTransport, new RoadTransport]
        );
    }

     /** Test get seat of transportation
     * return bool
     */

    public function testGetSeatMethod()
    {
        $stub = $this->getMockForAbstractClass('Transportation');
        $stub->expects($this->any())
             ->method('getSeat')
             ->will($this->returnValue(TRUE));


              // Define a closure that will call the protected method using "this".
        $seatCaller = function () {
            return $this->getSeat();
        };
        // Bind the closure to $foo's scope.
        $seatbound = $seatCaller->bindTo($stub, $stub);

        // Make a assertion.
        $this->assertTrue($seatbound());
    }

    /** Test get arriavl of transportation
     * return bool
     */

    public function testgetArrivalMethod()
    {
        $stub = $this->getMockForAbstractClass('Transportation');
        $stub->expects($this->any())
         ->method('getArrival')
         ->will($this->returnValue(TRUE));


          // Define a closure that will call the protected method using "this".
        $arrivalCaller = function () {
        return $this->getArrival();
        };
        // Bind the closure to $foo's scope.
        $arrivalbound = $arrivalCaller->bindTo($stub, $stub);

        // Make a assertion.
        $this->assertTrue($arrivalbound());
    }

    /** Test get departure of transportation
     * return bool
     */

    public function testgetdepartureMethod()
    {
        $stub = $this->getMockForAbstractClass('Transportation');
        $stub->expects($this->any())
             ->method('getDeparture')
             ->will($this->returnValue(TRUE));


              // Define a closure that will call the protected method using "this".
        $departureCaller = function () {
            return $this->getDeparture();
        };
        // Bind the closure to $foo's scope.
        $departurebound = $departureCaller->bindTo($stub, $stub);

        $this->assertTrue($departurebound());
    }


    /** Test get transport of transportation
     * return bool
     */

    public function testgettransportMethod()
    {
        $stub = $this->getMockForAbstractClass('Transportation');
        $stub->expects($this->any())
             ->method('gettransport')
             ->will($this->returnValue(TRUE));


              // Define a closure that will call the protected method using "this".
        $transportCaller = function () {
            return $this->gettransport();
        };
        // Bind the closure to $foo's scope.
        $transportbound = $transportCaller->bindTo($stub, $stub);

        $this->assertTrue($transportbound());
    }

    
    /** Test Cards loaded has subset 'asscoiative offset' ex;departure
     * return bool
     */
     
    public function testCardsIsValidSubsetFailure()
    {
        $this->assertArraySubset($this->cards,array(0=>array(0=>'departure')),'exists');
       
    }

    
    /** Test Cards loaded has valid array key
     * return bool
     */

    public function testCardsIsValidKey()
    {
        $this->assertArrayHasKey(0, $this->cards);
       
    } 



    /** Test mock the the outcome of order card lists
     * return bool
     */    

    public function testSomeBoardingcards() 
    {        

        // Create a stub for the SomeClass class.
        $stub = $this->createMock('sortboardingCards');

        // Configure the stub.
        $stub->method('listboardingCards')
             ->willReturn('foo');

        // Calling $stub->doSomething() will now return
        // 'foo'.
        $this->assertEquals('foo', $stub->listboardingCards());


    }

    /** Test mock the the outcome of order card lists with map stubs
     * return bool
     */ 

    public function testReturnValueMapStub()
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createMock('sortboardingCards');

        // Create a map of arguments to return values.
        $map = [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h']
        ];

        // Configure the stub.
        $stub->method('listboardingCards')
             ->will($this->returnValueMap($map));

        // $stub->doSomething() returns different values depending on
        // the provided arguments.
        $this->assertSame('k', $stub->listboardingCards('a', 'b', 'c'));
        $this->assertSame('h', $stub->listboardingCards('e', 'f', 'g'));
    }
    
}
