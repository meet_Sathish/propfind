<?php
/**
 * @author:sathish
 * @copyright:
 */
//namespace lib;
use Exception as Exception;

/**
  * Abstract Class Transportation
  *
  * This class is abstract and contains seat allocation, arrival, departure and baggage drop (if any) abstract properties defined
  * @abstracts setSeat, getSeat, setDeparture, getDeparture, getTransport, setTransport, setArrival and getArrival
  */
abstract class Transportation
{
	/** @var string|null Should contain a seat */
    Protected $seat;
	
	/** @var string|null Should contain a transport */
    Protected $transport;
	
	/** @var string|null Should contain a departure */
    Protected $departure;
	
	/** @var string|null Should contain a arrival */
    Protected $arrival;
	
	/** @var string|null Should contain a text */
    Protected $text;

     /**
      * This method sets seat.
      *
      * @abstract
      *
      */
    abstract protected function setSeat($seat);
	
     /**
      * This method gets seat.      
      *
      * @abstract
      */
    abstract protected function getSeat();
	
      /**
      * This method sets departure.
      *
      * @abstract
      *
      */
    abstract protected function setDeparture($departure);
	
     /**
      * This method gets departure.      
      *
      * @abstract
      */
    abstract protected function getDeparture();
	
     /**
      * This method sets arrival.
      *
      * @abstract
      */
    abstract protected function setArrival($arrival);
	
     /**
      * This method gets arrival.      
      *
      * @abstract
      */	  
    abstract protected function getArrival();   
	
      /**
      * This method sets transport.
      *
      * @abstract
      */
    abstract protected function setTransport($transport);
	
	
     /**
      * This method gets transport.     
      * @abstract
      */
    abstract protected function getTransport();   

}

/**
  * Class bustransport
  * 
  * @extends abstract Transportation
  *
  */
class bustransport extends Transportation
{
	 
    /**
     * Get seat 
     * @return string seat
     */
    public function getSeat()
    {
        return $this->seat;
    }
	
	 /**
     * set Seat 
     * @return string Seat
     */ 
    public function setSeat($seat)
    {
        $this->seat = $seat;
		return $this;
    }
	
	/**
     * Get departure 
     * @return string departure
     */
    public function getDeparture()
    {
        return $this->departure;
    }

	/**
     * Set departure 
     * @return string departure
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;
		return $this;
    }
	
	/**
     * Get arrival 
     * @return string arrival
     */
    public function getArrival()
    {
        return $this->arrival;
    }

	/**
     * Set arrival 
     * @return string arrival
     */
    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }
	
	/**
     * Get transport 
     * @return string transport
     */
	public function getTransport()
    {
      return $this->transport;
    }
    
	/**
     * Set transport 
     * @return string transport
     */
    public function setTransport($transport)
    {
      $this->transport = $transport;
    }
	
	/**
     * this method __toString
	 *
     * @return string 
     */
    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;
    }

}


/**
  * Class airtransport
  * 
  * @extends abstract Transportation
  *
  */
class airtransport extends Transportation
{
	
   /**
     * Get seat 
     * @return string seat
     */
    public function getSeat()
    {
        return $this->seat;
    }
	
	 /**
     * set Seat 
     * @return string Seat
     */ 
    public function setSeat($seat)
    {
        $this->seat = $seat;
		return $this;
    }
	
	/**
     * Get departure 
     * @return string departure
     */
    public function getDeparture()
    {
        return $this->departure;
    }

	/**
     * Set departure 
     * @return string departure
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;
		return $this;
    }
	
	/**
     * Get arrival 
     * @return string arrival
     */
    public function getArrival()
    {
        return $this->arrival;
    }

	/**
     * Set arrival 
     * @return string arrival
     */
    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }
	
	/**
     * Get transport 
     * @return string transport
     */
	public function getTransport()
    {
      return $this->transport;
    }
    
	/**
     * Set transport 
     * @return string transport
     */
    public function setTransport($transport)
    {
      $this->transport = $transport;
    }	
	
	/**
      * This method gets text.      
      *
      * @return string text
      */
	
    public function getText()
    {
       return $this->text;
    }


	/**
	* This method sets text.
	*
	* @param string text .
	*
	* @return string text
	*/
    public function setText($text)
    {
       $this->text = $text;
    }

	/**
     * this method __toString
	 *
     * @return string 
     */
    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat .", ". $this->text;
    }
}

/**
  * Class traintransport
  * 
  * @extends abstract Transportation
  *
  */
class traintransport extends Transportation
{
	
   /**
     * Get seat 
     * @return string seat
     */
    public function getSeat()
    {
        return $this->seat;
    }
	
	 /**
     * set Seat 
     * @return string Seat
     */ 
    public function setSeat($seat)
    {
        $this->seat = $seat;
		return $this;
    }
	
	/**
     * Get departure 
     * @return string departure
     */
    public function getDeparture()
    {
        return $this->departure;
    }

	/**
     * Set departure 
     * @return string departure
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;
		return $this;
    }
	
	/**
     * Get arrival 
     * @return string arrival
     */
    public function getArrival()
    {
        return $this->arrival;
    }

	/**
     * Set arrival 
     * @return string arrival
     */
    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }
	
	/**
     * Get transport 
     * @return string transport
     */
	public function getTransport()
    {
      return $this->transport;
    }
    
	/**
     * Set transport 
     * @return string transport
     */
    public function setTransport($transport)
    {
      $this->transport = $transport;
    }	
	
	/**
     * this method __toString
	 *
     * @return string 
     */
    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;
    }
}

/**
  * Class roadtransport
  * 
  * @extends abstract Transportation
  *
  */
class roadtransport extends Transportation
{
	
    /**
     * Get seat 
     * @return string seat
     */
    public function getSeat()
    {
        return $this->seat;
    }
	
	 /**
     * set Seat 
     * @return string Seat
     */ 
    public function setSeat($seat)
    {
        $this->seat = $seat;
		return $this;
    }
	
	/**
     * Get departure 
     * @return string departure
     */
    public function getDeparture()
    {
        return $this->departure;
    }

	/**
     * Set departure 
     * @return string departure
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;
		return $this;
    }
	
	/**
     * Get arrival 
     * @return string arrival
     */
    public function getArrival()
    {
        return $this->arrival;
    }

	/**
     * Set arrival 
     * @return string arrival
     */
    public function setArrival($arrival)
    {
        $this->arrival= $arrival;
    }
	
	/**
     * Get transport 
     * @return string transport
     */
	public function getTransport()
    {
      return $this->transport;
    }
    
	/**
     * Set transport 
     * @return string transport
     */
    public function setTransport($transport)
    {
      $this->transport = $transport;
    }	
	
	/**
     * this method __toString
	 *
     * @return string 
     */
    public function __toString()
    {
        return " Take ".$this->transport." from ".$this->departure." to ".$this->arrival. ", Seat ". $this->seat;
    }
}
?>
