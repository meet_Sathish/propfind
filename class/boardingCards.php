<?php
/**
 * @author:sathish
 * @copyright:
 */
//namespace lib;
use Exception as Exception;

/**
 * Abstract Class boardingcards
 * @abstracts getcards and setcards
 */
abstract class boardingCards
{
    /**
     * Cards array
     * @var $cards 
     */
    protected $cards;
    /**
     * abstract get set cards
     * @return array cards
     */
    abstract public function getCards();
    /**
     * abstract get set cards
     * @return null
     */
    abstract public function setCards($cards);
    
}
/**
 * Class sortboardingCards
 * @extends abstract boardingcards
 */
class sortboardingCards extends boardingCards
{
    
    
    /**
     * LoadJson to load unordered cards
     * @return type
     */
    public function __construct($filename)
    {
        
        $journeyString = @file_get_contents($filename);
        $this->cards = json_decode($journeyString, true);
        if ($this->cards == null) {
            throw new InvalidArgumentException("JSON Invalid");
        }
        
    }
   
    /**
     * Get Cards 
     * @return array cards
     */
    public function getCards()
    {
        return $this->cards;
    }
    /**
     * Set Cards 
     * @return array Cards
     */
    public function setCards($cards)
    {
        $this->cards = $cards;
        return $this;
    }
   
    /**
     * listboardingcads
     * @return string
     */

    public function listboardingCards()
    {

        //Initializing

        //create Bus transport
        $busmode = new bustransport();

        //create Air transport
        $airmode = new airtransport();

        //create Traintransport
        $trainmode = new traintransport();

        //create Road transport
        $trainmode = new roadtransport();

        $inc = 1;

        $boardingPassesFilter = [];

        if (count($this->cards)) 
        {
            $boardingPassesFilter['failedLeg']  ="";

            //To find at least departure and arrival colummns that occurs in boarding cards once (odd edges fidning in vertex)
            foreach ($this->cards as $value) 
            {
                if (!in_array($value['arrival'], array_column($this->cards, 'departure'))) {
                    $boardingPassesFilter['arrival'] = $value['arrival'];

                    if (!in_array($value['departure'], array_column($this->cards, 'arrival'))) {
                        $boardingPassesFilter['failedLeg'] = $value['departure'];
                    }
                }

                if (!in_array($value['departure'], array_column($this->cards, 'arrival'))) {
                    $boardingPassesFilter['departureTransit'] = $value['departure'];
                }

            }

             // Loop till departure was not arrival on cards with temporary transit set
            if ( $boardingPassesFilter['departureTransit'] && $boardingPassesFilter['arrival'] && $boardingPassesFilter['failedLeg'] == "")
            {

                while ($boardingPassesFilter['departureTransit'] != $boardingPassesFilter['arrival']) 
                {
                    foreach ($this->cards as $index => $path) 
                    {
                        // Move Pointer if boarding card arrival matches next pointer departure
                        if ($path['departure'] == $boardingPassesFilter['departureTransit']) 
                        {

                            switch ($path['mode'])
                            {

                                case "bus";
                                    $mode = $busmode;
                                break;
                                case "air";
                                    $mode = $airmode;
                                    $mode->setText($path['text']);
                                break;
                                case "train";
                                    $mode = $trainmode;
                                break;
                                    $mode = $roadmode;
                                break;

                            }

                            $mode->setSeat($path['seat']);
                            $mode->setTransport($path['transport']);
                            $mode->setDeparture($path['departure']);
                            $mode->setArrival($path['arrival']);

                            echo $inc .")" .$mode."<br>";
                            
                            $boardingPassesFilter['departureTransit'] = $path['arrival'];
                            
                            unset($this->cards[$index]);
                            $inc++;
                        }         

                    }
                }
            }
            else if ($boardingPassesFilter['failedLeg'])
            {
                echo "<span style='color:red;font-weight:bold;'>A broken trip '".$boardingPassesFilter['failedLeg'] ."' found </span><br>";
                var_dump($this->cards);
                exit;
            }
            
            echo $inc .") You have reached final destination \n";
            
        }
        else
        {
            echo "cards empty";
        }

    }
}



